/// <reference path="../_ts.d.ts" />
import Bindings from './bindings';

export default class Component extends Bindings {
    private config = {};

    constructor(options: any) {
        super();

        this.config = options;
    }

}