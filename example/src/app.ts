import { Http, Render, Router, Component } from "../../src/"

import About from "./components/about/about";
export default class myApp {
    public http: Http;
    public router: Router;

    constructor() {
        this.run();
    }


    run() {
        this.router.route([{
            route: "/about",
            component: About,
        }]);
    }


}

new myApp();

let myCustomComponent = new Component({
    option: true
});

